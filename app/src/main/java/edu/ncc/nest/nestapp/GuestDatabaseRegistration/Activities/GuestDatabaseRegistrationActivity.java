package edu.ncc.nest.nestapp.GuestDatabaseRegistration.Activities;

/**
 *
 * Copyright (C) 2020 The LibreFoodPantry Developers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;


import java.util.Locale;
import edu.ncc.nest.nestapp.Choose;
import edu.ncc.nest.nestapp.R;


/**
 * GuestDatabaseRegistrationActivity: This is the underlying activity for the fragments of the
 * GuestDatabaseRegistration feature.
 */
public class GuestDatabaseRegistrationActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Clear the back data for every new activity cycle of guest registration
        deleteSharedPreferences("BackFrag2");
        deleteSharedPreferences("BackFrag3");
        deleteSharedPreferences("BackFrag5");

        setContentView(R.layout.activity_guest_database_registration);

        setSupportActionBar(findViewById(R.id.database_registration_toolbar));

        //Spinner for Language translator
        Spinner spinner = findViewById(R.id.language_spinner);
        ArrayAdapter<CharSequence> arrayAdapter=ArrayAdapter.createFromResource(this,R.array.localestrings, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i==1){
                    setLocale("en");
                    startActivity(getIntent());
                } else if (i == 2) {
                    setLocale("es");
                    startActivity(getIntent());
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;

    }

    /**
     * Finishes the current activity --GuestDatabaseregistration-- and goes to the homepage
     * {@link Choose} Activity
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.home_btn)

            finish();

        return super.onOptionsItemSelected(item);

    }

    /*
    /**
     * home --
     * Starts the {@link Choose} Activity
     */
    /*
    public void home() {

        Intent intent = new Intent(this, Choose.class);

        startActivity(intent);

    }

     */

    // SetLocale to English or Spanish
    public void setLocale(String language) {
        Locale myLocale = new Locale(language);
        Resources res = this.getResources();
        DisplayMetrics display = res.getDisplayMetrics();
        Configuration configuration = res.getConfiguration();
        configuration.locale = myLocale;
        res.updateConfiguration(configuration, display);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
    }

    @Override
    protected void onRestart()
    {
        super.onRestart();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
    }

    @Override
    protected void onDestroy()
    {
        deleteSharedPreferences("BackFrag2");
        deleteSharedPreferences("BackFrag3");
        deleteSharedPreferences("BackFrag5");
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) { super.onSaveInstanceState(outState); }

    @Override
    public void onRestoreInstanceState(Bundle inState)
    {
        super.onSaveInstanceState(inState);
    }
}
