package edu.ncc.nest.nestapp.reports.Fragments;
/**
 *
 * Copyright (C) 2023 The LibreFoodPantry Developers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.time.LocalDate;
import java.time.Month;

import edu.ncc.nest.nestapp.GuestVisit.DatabaseClasses.QuestionnaireHelper;
import edu.ncc.nest.nestapp.GuestVisit.DatabaseClasses.QuestionnaireSource;
import edu.ncc.nest.nestapp.R;

public class QuestReportFragment extends Fragment implements View.OnClickListener {

    Spinner ageSpinner, monthSpinner, guestSpinner;
    Button monthBtn, ageBtn, guestBtn, downloadBtn;
    LinearLayout monthPrompt, agePrompt, guestPrompt;
    TextView guestTxt, guestCount, familyTxt, familyCount, ageTxt, ageCount, visitTxt, visitCount;
    EditText guestIdPromt;
    // the database for the Questinare .
    QuestionnaireSource questDB;

    public QuestReportFragment() {
        super(R.layout.fragment_quest_report);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_quest_report, container, false);

        questDB = new QuestionnaireSource(this.getContext());



        monthSpinner = (Spinner) rootView.findViewById(R.id.month_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getContext(),
                R.array.months_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        monthSpinner.setAdapter(adapter);

        ageSpinner = (Spinner) rootView.findViewById(R.id.age_spinner);
        adapter = ArrayAdapter.createFromResource(this.getContext(),
                R.array.age_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ageSpinner.setAdapter(adapter);

        monthBtn = (Button) rootView.findViewById(R.id.month_selected_go_btn);
        monthBtn.setOnClickListener(this);

        ageBtn = (Button) rootView.findViewById(R.id.age_selected_go_btn);
        ageBtn.setOnClickListener(this);

        guestBtn = (Button) rootView.findViewById(R.id.visitor_selected_go_btn);
        guestBtn.setOnClickListener(this);

        downloadBtn = (Button) rootView.findViewById(R.id.download_data);
        downloadBtn.setOnClickListener(this);


        guestTxt = (TextView) rootView.findViewById(R.id.guest_served_txt);
        guestCount = (TextView) rootView.findViewById(R.id.guest_served_count_txt);
        familyTxt = (TextView) rootView.findViewById(R.id.family_served_txt);
        familyCount = (TextView) rootView.findViewById(R.id.family_served_count_txt);
        ageTxt = (TextView) rootView.findViewById(R.id.age_served_txt);
        ageCount = (TextView) rootView.findViewById(R.id.age_served_count_txt);
        visitTxt = (TextView) rootView.findViewById(R.id.visitor_served_txt);
        visitCount = (TextView) rootView.findViewById(R.id.visitor_served_count_txt);

        guestIdPromt = (EditText) rootView.findViewById(R.id.visitor_text);

        monthPrompt = (LinearLayout) rootView.findViewById(R.id.month_prompt);
        agePrompt = (LinearLayout) rootView.findViewById(R.id.age_prompt);
        guestPrompt = (LinearLayout) rootView.findViewById(R.id.visitor_prompt);









        return rootView;
    }

    @Override
    public void onClick(View v) {
        int curYear = LocalDate.now().getYear();
        int month = Integer.parseInt(monthSpinner.getSelectedItem().toString());
        String guestIdAnswer = guestIdPromt.getText().toString();
        int ageSelected = ageSpinner.getSelectedItemPosition();
        questDB.open();
        switch (v.getId()) {

            case R.id.month_selected_go_btn:// TODO test the Query methods to make sure it works

                //getting the element from the spinner and assigning to month

                int numOfGuestCount =  questDB.getTotalVisitors(month,curYear);
                int numOfFamServed = questDB.getTotalServed(month,curYear);
                guestTxt.setVisibility(View.VISIBLE);
                guestCount.setVisibility(View.VISIBLE);
                //updated the guestCount according to the data we pull from DB
                guestCount.setText("here this is the total guest"+numOfGuestCount);


                familyTxt.setVisibility(View.VISIBLE);
                familyCount.setVisibility(View.VISIBLE);
                //TODO update familyCount text to pull from DB
                familyCount.setText("this is the family size "+numOfFamServed);

                monthPrompt.setVisibility(View.GONE);
                agePrompt.setVisibility(View.VISIBLE);
                guestPrompt.setVisibility(View.VISIBLE);
                guestIdPromt.setVisibility(View.VISIBLE);
                questDB.close();

                break;
            case R.id.age_selected_go_btn:
                Log.d("hey", "onCreateView: "+ageSpinner.getSelectedItemPosition());

                int totalServedByAge =  questDB.getTotalServedByAge(month,curYear,ageSelected);
                ageTxt.setVisibility(View.VISIBLE);
                ageCount.setVisibility(View.VISIBLE);
                //TODO update ageCount text to pull from DB

                agePrompt.setVisibility(View.GONE);
                questDB.close();

                break;
            case R.id.visitor_selected_go_btn:
                int numOfPersonVisited = questDB.getTotalPersonVisits(month,curYear,guestIdAnswer);
                visitTxt.setVisibility(View.VISIBLE);
                visitCount.setVisibility(View.VISIBLE);
                visitCount.setText("hey this is person visited"+numOfPersonVisited);
                questDB.close();



                downloadBtn.setVisibility(View.VISIBLE);

                //TODO update visitCount text to pull from DB

                guestPrompt.setVisibility(View.GONE);
                break;
        }
    }
}
