package edu.ncc.nest.nestapp.GuestDatabaseRegistration.Fragments;

/**
 * Copyright (C) 2020 The LibreFoodPantry Developers.
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentResultListener;

import java.time.LocalDate;
import edu.ncc.nest.nestapp.CheckExpirationDate.Activities.CheckExpirationDateActivity;

import edu.ncc.nest.nestapp.GuestDatabaseRegistration.Activities.GuestDatabaseRegistrationActivity;
import edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistryHelper;
import edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistrySource;
import edu.ncc.nest.nestapp.GuestFormTesting;
import edu.ncc.nest.nestapp.GuestVisit.Activities.GuestVisitActivity;
import edu.ncc.nest.nestapp.R;
import edu.ncc.nest.nestapp.databinding.FragmentGuestDatabaseRegistrationSummaryBinding;

/**
 * SummaryFragment: This fragment represent a summary of the registration process. Displays messages
 * to the guest depending on whether or not the registration process was successful or not. Should
 * also generate a barcode for the guest if needed.
 */
public class SummaryFragment extends Fragment {
    private FragmentGuestDatabaseRegistrationSummaryBinding binding;
    private OnBackPressedCallback backbuttonCallback;

    // Instance variables for the FirstFormFragment
    private String fname; //First Name
    private String lname; //Last Name
    private String phoneNum; //Phone Number
    private String nccId; //NCCID

    // Instance variables for the SecondFormFragment
    private String streetAddress1;
    private String streetAddress2;
    private String city;
    private String state;
    private String zip;
    private String affiliation;
    private String age;
    private String gender;

    // Instance variables for the ThirdFormFragment
    private String dietary; //Dietary needs
    private String snap; //If they have snap
    private String otherProg; // if they know about any other programs other then nest
    private String employment; // Employment information
    private String health; //health information
    private String housing; // housing status
    private String income;

    // Instance variables for the FourthFormFragment
    private String householdNum;
    private String children1;
    private String children5;
    private String children12;
    private String children18;

    // barcode info
    private String barcode;

    /*
     *-DEPREICATED PAGE HAS BEEN REMOVED FROM INITIAL REGISTRATION AS OF 5/15/2023-
    // Instance variables for the FifthFormFragment
    private String referrer;
    private String comments;
    private String volunteerFName;
    private String volunteerLName;
    */

    // guest last visit - date of registration
    private String lastVisit;

    // Database where we will store user information
    private GuestRegistrySource db;

    //LogD Tag
    public static final String TAG = SummaryFragment.class.getSimpleName();

    private boolean doneButtonClicked = false;
    private boolean regButtonClicked = false;
    private boolean checkInButtonClicked = false;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentGuestDatabaseRegistrationSummaryBinding.inflate(inflater, container, false);

        Log.d(TAG, "In SummaryFragment onCreateView()");

        return binding.getRoot();
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // override back button to give a warning
        backbuttonCallback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                // show dialog prompting user
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setCancelable(false);
                builder.setTitle("Are you sure?");
                builder.setMessage("Data entered on this page may not be saved.");
                // used to handle the 'confirm' button
                builder.setPositiveButton("Yes, I'm Sure", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // continue by disabling this callback then calling the backpressedispatcher again
                        // when this was enabled, it was at top of backpressed stack. When we disable, the next item is default back behavior
                        backbuttonCallback.setEnabled(false);
                        requireActivity().getOnBackPressedDispatcher().onBackPressed();
                    }
                });
                // handles the 'cancel' button
                builder.setNegativeButton("Stay On This Page", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel(); // tells android we 'canceled', not dismiss. more appropriate
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();

            }
        };
        // need to add the callback to the activities backpresseddispatcher stack.
        // if enabled, it will run this first. If disabled, it will run the default (next item in stack)
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), backbuttonCallback);

        // TODO do we need a back verification here? prob not if fields are not editable. otherwise copy/paste from previous fragments

        // Creating the database and passing the correct context as the argument
        db = new GuestRegistrySource(requireContext());

        //retrieving first name, last name, phone number and NCC ID from FirstFormFragment bundle.
        getParentFragmentManager().setFragmentResultListener("sending_first_form_fragment_info",
                this, new FragmentResultListener() {
                    @Override
                    public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {

                        //retrieving information & assigning to instance variables
                        fname = result.getString("First Name");
                        lname = result.getString("Last Name");
                        phoneNum = result.getString("Phone Number");

                        //LogD
                        Log.d(TAG, "The first name obtained is: " + fname);
                        Log.d(TAG, "The last name is: " + lname);
                        Log.d(TAG, "The phone number is: " + phoneNum);

                        //setting summary fragment textviews
                        binding.grf1FName.setText(fname);
                        binding.grf1LName.setText(lname);
                        binding.grf1Phone.setText("(" + phoneNum.substring(0, 3) + ") " + phoneNum.substring(3, 6) + "-" + phoneNum.substring(6));
                    }
                });//end retrieving FirstFormFragement

        //retrieving streetAddress1&2, city, state zip, affiliation, age, and gender from SecondFormFragment bundle.
        getParentFragmentManager().setFragmentResultListener("sending_second_form_fragment_info",
                this, new FragmentResultListener() {
                    @Override
                    public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {

                        //retrieving information & assigning to instance variables
                        streetAddress1 = result.getString("Street Address 1");
                        streetAddress2 = result.getString("Street Address 2");  //optional
                        city = result.getString("City");
                        state = result.getString("State");
                        zip = result.getString("Zip");
                        affiliation = result.getString("Affiliation");
                        age = result.getString("Age");
                        gender = result.getString("Gender");
                        nccId = result.getString("NCC ID");

                        //LogD
                        Log.d(TAG, "The street address 1: " + streetAddress1);
                        Log.d(TAG, "The street address 2 (optional): " + streetAddress2);
                        Log.d(TAG, "The city is: " + city);
                        Log.d(TAG, "The state is: " + state);
                        Log.d(TAG, "The zip code is: " + zip);
                        Log.d(TAG, "The affiliation is: " + affiliation);
                        Log.d(TAG, "The age is: " + age);
                        Log.d(TAG, "The gender is: " + gender);
                        Log.d(TAG, "The NCC ID is: " + nccId);

                        //setting summary fragment textview streetaddress
                        binding.grf2Address1.setText(streetAddress1);
                        //if street adress 2 was subbmitted then assign to street address 2
                        if (streetAddress2 != null)
                            binding.grf2Address2.setText(streetAddress2);
                        //setting the rest of the summary fragment textviews
                        binding.grf2City.setText(city);
                        binding.grf2State.setText(state);
                        binding.grf2Zip.setText(zip);
                        binding.grf2Affiliation.setText(affiliation);
                        binding.grf2Age.setText(age);
                        binding.grf2Gender.setText(gender);
                        if(nccId.equals("N/A"))
                            binding.grf2NccId.setText(nccId);
                        else
                            binding.grf2NccId.setText("N" + nccId);


                    }
                });//end retrieving SecondFormFragement


        // retrieving dietary, other programs, snap, employment, health, and housing info from ThirdFormFragment bundle.
        getParentFragmentManager().setFragmentResultListener("sending_third_form_fragment_info",
                this, new FragmentResultListener() {
                    @Override
                    public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {

                        //retrieving information & assigning to instance variables
                        dietary = result.getString("dietary");
                        snap = result.getString("snap");
                        otherProg = result.getString("programs");
                        employment = result.getString("employment");
                        health = result.getString("health");
                        housing = result.getString("housing");
                        income = result.getString("income");

                        //LogD
                        Log.d(TAG, "The dietary information is: " + dietary);
                        Log.d(TAG, "The snap information is: " + snap);
                        Log.d(TAG, "The other program information is: " + otherProg);
                        Log.d(TAG, "The employment obtained is: " + employment);
                        Log.d(TAG, "The health information is: " + health);
                        Log.d(TAG, "The hosuing information is: " + housing);
                        Log.d(TAG, "The income information is: " + income);

                        /*setting summary fragment textviews
                         *format of setText(replaceAll) is:
                         * \nBOLD LABEL:
                         * Text 1
                         * Text 2
                         * Text 3
                         */
                        binding.grf3Dietary.setText(dietary.replaceAll(", ", "\n"));
                        binding.grf3Snap.setText("\n" + snap);
                        binding.grf3OtherProgs.setText(otherProg);
                        binding.grf3StatusEmployment.setText(employment.replaceAll(", ", "\n"));
                        binding.grf3StatusHealth.setText(health.replaceAll(", ", "\n"));
                        binding.grf3StatusHousing.setText(housing.replaceAll(", ", "\n"));
                        binding.grf3Income.setText("\n" + income);

                    }
                });//end retrieving ThirdFormFragement

        // retrieving household number, childcare status, and age info of children from FourthFormFragment bundle.
        getParentFragmentManager().setFragmentResultListener("sending_fourth_form_fragment_info",
                this, new FragmentResultListener() {
                    @Override
                    public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {

                        //retrieving information & assigning to instance variables
                        householdNum = result.getString("householdNum");
                        children1 = result.getString("children1");
                        children5 = result.getString("children5");
                        children12 = result.getString("children12");
                        children18 = result.getString("children18");

                        //LogD
                        Log.d(TAG, "The house hold number is: " + householdNum);
                        Log.d(TAG, "The children 1 is: " + children1);
                        Log.d(TAG, "The children 5 is: " + children5);
                        Log.d(TAG, "The children 12 is: " + children12);
                        Log.d(TAG, "The children 18 is: " + children18);

                        //setting summary fragment textviews
                        binding.grf4NumPeople.setText(householdNum);
                        binding.grf4Children1.setText(children1);
                        binding.grf4Children5.setText(children5);
                        binding.grf4Children12.setText(children12);
                        binding.grf4Children18.setText(children18);

                    }
                });//end retrieving FouthFormFragement

        getParentFragmentManager().setFragmentResultListener("sending_barcode_info",
                this,
                new FragmentResultListener() {
                    @Override
                    public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                        barcode = result.getString("registrationBarcode");

                        binding.edittextEnterUpc.setText(barcode);
                        Log.d("**BARCODE**", "BARCODE: " + barcode);
                    }
                });//end retrieving FirstFormFragement

       /*-DEPREICATED PAGE HAS BEEN REMOVED FROM INITIAL REGISTRATION AS OF 5/15/2023-
        getParentFragmentManager().setFragmentResultListener("sending_fifth_form_fragment_info",
                this, new FragmentResultListener() {
                    @Override
                    public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                        referrer = result.getString("Referrer");
                        comments = result.getString("Comments");
                        volunteerFName = result.getString("Volunteer First Name");
                        volunteerLName = result.getString("Volunteer Last Name");

                        binding.grf5Referrer.setText(referrer);
                        binding.grf5Comments.setText(comments);
                        binding.grf5VolunteerFName.setText(volunteerFName);
                        binding.grf5VolunteerLName.setText(volunteerLName);
                    }
                }); */


        // OnClickListener for the "Done" button

        view.findViewById(R.id.button_done_summary).setOnClickListener(clickedView -> {
                    /* is there a way to use the saveToDatabase method here and streamline this code?
                     * see code commented out below around line 394*/
                    // registering the guest to the database
                    // TODO: null values needs to be retrieved and replaced.
                    //String nameOfVolunteer = volunteerFName + " " + volunteerLName;
                    lastVisit = LocalDate.now().toString(); // last visit date will start as the date of registration

                    doneButtonClicked = true;
                    saveToDatabase();
            /*
                    if (db.isConnected()) {
                        db.insertData(fname + " " + lname, phoneNum, nccId, streetAddress1 + ", " + streetAddress2,
                                city, zip, state, affiliation, age, gender, dietary, otherProg, snap, employment, health, housing,
                                income, householdNum, children1, children5, children12, children18, referrer, comments, nameOfVolunteer, barcode, lastVisit);


                    } else {

                        //Opens Local Database
                        //maybe use requirecontext instead of getcontext
                        GuestRegistryHelper localRegDatabase = new GuestRegistryHelper(getContext());
                        LocalDate todaysDate = LocalDate.now();
                        lastVisit = todaysDate.toString();
                        Log.d(TAG, "todays date: " + lastVisit);
                        doneButtonClicked = true;
                        saveToDatabase();

                        boolean addedData = localRegDatabase.addToLocalGRDataBase(fname + " " + lname, phoneNum, nccId, streetAddress1 + ", " + streetAddress2,
                                city, zip, state, affiliation, age, gender, dietary, otherProg, snap, employment, health, housing,
                                income, householdNum, children1, children5, children12, children18,
                                referrer, comments, nameOfVolunteer, barcode, lastVisit);


                        if (addedData) {
                            Log.d(TAG, "data successfully added to localdatabase");
                        } else {
                            Log.d(TAG, "onViewCreated: data NOT added to localdatabase!!!");
                        }


                    }
                    */
                    // go back to 'guest forms' page. decided this makes more sense than app home.
                    // see method definition below for explanation how to switch to to navigate to app home instead
                    createConfirmedDialog(); // show a dialog first so they know it worked.
                });

            // Navigate back to splash screen.
            // later, make if/else to go to scanner or login if scanner already in db
            /*
            NavHostFragment.findNavController(SummaryFragment.this)
                    .navigate(R.id.action_DBR_SummaryFragment_to_DBR_StartFragment);

             */


        // OnClickListener for the "Check-in" button
        view.findViewById(R.id.button_check_in).setOnClickListener(clickedView -> {
            checkInButtonClicked = true;
            saveToDatabase();
        });
        // OnClickListener for the "Done" button

        view.findViewById(R.id.button_registration).setOnClickListener(clickedView -> {
            regButtonClicked = true;
            saveToDatabase();
        });

        /*
        // OnClickListener for the "Done" button
        //TODO store in database when done button is clicked
          view.findViewById(R.id.button).setOnClickListener(clickedView -> {

              // Navigate back to splash screen.
              // later, make if/else to go to scanner or login if scanner already in db
              NavHostFragment.findNavController(SummaryFragment.this)
                      .navigate(R.id.action_DBR_SummaryFragment_to_DBR_StartFragment);
          });
        */
    }

    /**
     * home method --
     * description: this method goes to the nest home screen
     */
    public void home() {
        // sending to guestform testing so they can login. Otherwise switch to "Choose.class" to go to app home.
        Intent intent = new Intent(getActivity(), GuestFormTesting.class);
        startActivity(intent);
    }

    public boolean createConfirmedDialog() {
        //creates the dialog prevents it from being canceled
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(false);

        //sets the title
        builder.setTitle("Thank you!");

        //sets the main text accordingly
        if (doneButtonClicked)
            builder.setMessage("Your registration has been confirmed. " +
                    "You will now be taken back to the Home Page. " +
                    "You can now log in and start your first visit!");
        else if (regButtonClicked)
            builder.setMessage("Your registration has been confirmed. " +
                    "You will now be taken back to the Registration Page.");
        else
            builder.setMessage("Your registration has been confirmed. " +
                    "You will now be taken to the Check-in Page. " +
                    "You can now log in and start your first visit!");

        // close button
        builder.setPositiveButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //close the dialog
                dialog.dismiss();

                if (doneButtonClicked) { //if the "done" button is clicked send user to home screen
                    getActivity().finish();
                    Log.d(TAG, "onClick: done button clicked");
                } else if (regButtonClicked) { //if "Registration button clicked send user to registration
                    getActivity().finish();
                    launchGuestReg();
                    Log.d(TAG, "onClick: reg button clicked");
                } else if (checkInButtonClicked) { //if "Check-in" button is clicked send user to Check in
                    getActivity().finish();
                    launchGuestVisit();
                    Log.d(TAG, "onClick: checkin button clicked");
                } else { //if something UNEXPECTED happens send user to home screen
                    getActivity().finish();
                    Log.d(TAG, "onClick:NOTHING clicked");
                }
            }
        });
        //opens the dialog
        builder.show();
        return true;
    }

    /**
     * launchGuestVisit - starts the GuestVisitActivity activity
     */
    public void launchGuestVisit() {
        Intent intent = new Intent(requireActivity(), GuestVisitActivity.class);
        startActivity(intent);
    }

    /**
     * launchGuestDatabaseRegistrationActivity - starts the GuestDatabaseRegistrationActivity activity
     */
    public void launchGuestReg() {
        Intent intent = new Intent(requireActivity(), GuestDatabaseRegistrationActivity.class);
        startActivity(intent);
    }

    /**
     * launchGetUPC - starts the Get UPC activity
     */
    public void launchGetUPC() {
        Intent intent = new Intent(requireActivity(), CheckExpirationDateActivity.class);
        startActivity(intent);
    }

    /**
     * saveToDatabase- Saves to which ever database its able to connect to
     */
    public void saveToDatabase() {
        // TODO: null values needs to be retrieved and replaced.

        // THESE ARE DEPRECATED, DEFAULT VALUES ARE "DEPRECATED"
        // TODO: REMOVE THESE COLLUMS FROM THE DATABASE
        String referrer = "DEPRECATED";
        String comments = "DEPRECATED";
        String volunteerFName = "DEPRECATED";
        String volunteerLName = "DEPRECATED";
        String nameOfVolunteer = volunteerFName + " " + volunteerLName;

        LocalDate todaysDate = LocalDate.now();
        lastVisit = todaysDate.toString();
        Log.d(TAG, "todays date: " + lastVisit);

        //connecting to database
        if (db.isConnected()) {
            db.insertData(fname + " " + lname, phoneNum, nccId, streetAddress1 + ", " + streetAddress2,
                    city, zip, state, affiliation, age, gender, dietary, otherProg, snap, employment, health, housing,
                    income, householdNum, children1, children5, children12, children18, referrer, comments, nameOfVolunteer,
                    barcode, lastVisit);
        } else {
            //Opens Local Database
            GuestRegistryHelper localRegDatabase = new GuestRegistryHelper(getContext());


            //adds to local databse
            boolean addedData = localRegDatabase.addToLocalGRDataBase(fname + " " + lname, phoneNum, nccId, streetAddress1 + ", " + streetAddress2,
                    city, zip, state, affiliation, age, gender, dietary, otherProg, snap, employment, health, housing,
                    income, householdNum, children1, children5, children12, children18,
                    referrer, comments, nameOfVolunteer, barcode, lastVisit);

            //checks if the data was added successfully or not
            if (addedData) {
                Log.d(TAG, "data successfully added to LOCAL DATABASE");
            } else {
                Log.d(TAG, "onViewCreated: data --NOT-- added to LOCAL DATABASE!");
            }

        }

        //show dialog that its completed
        createConfirmedDialog();
    }


}