/**
 *
 * Copyright (C) 2023 The LibreFoodPantry Developers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


package edu.ncc.nest.nestapp.reports.Fragments;

import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.Calendar;
import java.util.Date;

import edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistrySource;
import edu.ncc.nest.nestapp.GuestVisit.DatabaseClasses.QuestionnaireHelper;
import edu.ncc.nest.nestapp.GuestVisit.DatabaseClasses.QuestionnaireSource;
import edu.ncc.nest.nestapp.R;

public class RegReportFragment extends Fragment {
    private GuestRegistrySource dbGuestSource;
    private QuestionnaireSource dbQuestionSource;


    TextView totalGuestCount;
    TextView newGuestCount;

    public RegReportFragment() {
        super(R.layout.fragment_reg_report);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reg_report, container, false);
        totalGuestCount =  rootView.findViewById(R.id.total_guest_count_txt);//getting the id's
        newGuestCount  = rootView.findViewById(R.id.new_guest_count_txt);//getting the id's

        //Query the Db so it Shows tje Total guest registered to the NEST.
        dbGuestSource = new GuestRegistrySource(this.getContext());

        //testing the getTotalGuests()
        int numOfVisitors = dbGuestSource.getTotalGuests();

        //a way to get the current month to display the Total num
        Month curMonth = LocalDate.now().getMonth();
        int month = curMonth.getValue();
        int curYear = LocalDate.now().getYear();

        //testing gto see if the we are in the current date and year.
        Log.d("TAG", "onCreateView: month is :"+month +" the year is :"+curYear);

        //this is hardcoded now to test the query method
        int numOfTotalVisitors = dbGuestSource.getMonthlyGuests(month,curYear);

        //set the Text to the information given.
        totalGuestCount.setText(""+numOfVisitors);
        newGuestCount.setText(""+numOfTotalVisitors);

        return rootView;
    }



}
