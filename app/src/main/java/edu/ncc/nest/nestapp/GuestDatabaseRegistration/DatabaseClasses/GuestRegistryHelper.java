package edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses;

/**
 *
 * Copyright (C) 2019 The LibreFoodPantry Developers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * NESTGuestRegistry.db
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.icu.text.SimpleDateFormat;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.Locale;

/**
 * GuestRegistryHelper: Handles the creation and upgrade of the GuestRegistry database.
 */
public class GuestRegistryHelper extends SQLiteOpenHelper {

    // The URI scheme used for content URIs
    public static final String SCHEME = "content";

    // The provider's authority
    public static final String AUTHORITY = "edu.ncc.zqk.nestguestforms";

    public static final Uri CONTENT_URI = Uri.parse(SCHEME + "://" + AUTHORITY);

    // Table name
    public static final String TABLE_NAME = "NESTGuestRegistry";

    // Columns in the table
    public static final String _ID = "_id";

    //first fragment information
    public static final String NAME = "name";
    public static final String PHONE = "phone";
    public static final String NCC_ID = "nccID";
    public static final String DATE = "date";

    //second fragment information
    public static final String ADDRESS = "address";
    public static final String CITY = "city";
    public static final String ZIP = "zipcode";
    public static final String STATE = "state";
    public static final String AFFILIATION = "affiliation";
    public static final String AGE = "age";
    public static final String GENDER = "gender";

    //third fragment information
    public static final String DIET = "diet";
    public static final String PROGRAMS = "programs";
    public static final String SNAP = "snap";
    public static final String EMPLOYMENT = "employment";
    public static final String HEALTH = "health";
    public static final String HOUSING = "housing";
    public static final String INCOME = "income";

    //fourth fragment information
    public static final String HOUSEHOLD_NUM = "householdNum";
    //public static final String CHILDCARE_STATUS = "childcareStatus";
    public static final String CHILDREN_1 = "children1";
    public static final String CHILDREN_5 = "children5";
    public static final String CHILDREN_12 = "children12";
    public static final String CHILDREN_18 = "children18";
    public static final String BARCODE = "barcode";
    public static final String LASTVISIT = "lastVisit";


    /* REMOVED AS OF 5/15/2023
    //TODO: additional data to be added when the data when UI has it
    public static final String  REFERRAL_INFO = "refInfo";
    public static final String ADDITIONAL_INFO = "addInfo";
    public static final String NAME_OF_VOLUNTEER = "nameOfVolunteer";
    public static final String BARCODE = "barcode";
     */

    // Seperate Table for guest check in table Dsolash 12/5 12:05pm, Maybe not needed
    /*
    public static final String TABLE_NAME_CI = "NESTCheckInDatabase";
    public static final String DATE_CI = "Visit Date";
    public static final String Counter_CI = "Num Visits";
    public static final String SENIORS_CI = "Num Seniors";
    public static final String ADULTS_CI = "Num Adults";
    public static final String TOTAL_CI = "Total Household Size";
    public static final String CHILDREN_CI = "Num Children";
    */




    // Database version and name
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "GuestRegistry.db";

    public GuestRegistryHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        /*
                For readability: Each fragment's data group is separated by the + in each new line in the arguments.
                Refer back to the public static String declarations to see what each of the four fragment's data groups contains.
                Here is the general syntax of what it looks like
                db.execSQL(........
                    +(first form fragment info)
                    +(second form fragment info)
                    more data for second form so its not all contained in a single line
                    +(third form fragment info)
                    more data for third form so its not all contained in a single line
                    +(fourth form fragment info)
                    more data for fourth form so its not all contained in a single line
                    +(additional data)
         */
        db.execSQL("CREATE TABLE "
                + TABLE_NAME + " ("
                + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + NAME + " TEXT, "
                + PHONE + " TEXT, "
                + NCC_ID + " TEXT, "
                + DATE + " TEXT, "
                + ADDRESS + " TEXT, "
                + CITY + " TEXT, "
                + ZIP + " TEXT, "
                + STATE + " TEXT, " +
                AFFILIATION + " TEXT, "
                + AGE + " TEXT, "
                + GENDER + " TEXT, "
                + DIET + " TEXT, "
                + PROGRAMS + " TEXT, "
                + SNAP + " TEXT, "
                + EMPLOYMENT + " TEXT, "
                + HEALTH + " TEXT, "
                + HOUSING + " TEXT, "
                + INCOME + " TEXT, "
                + HOUSEHOLD_NUM + " TEXT, "
                + CHILDREN_1 + " TEXT, "
                + CHILDREN_5 + " TEXT, "
                + CHILDREN_12 + " TEXT, "
                + CHILDREN_18 + " TEXT, "
                + BARCODE + " TEXT, "
                + LASTVISIT + " TEXT);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        onCreate(db);

    }

    /**
     * addToLocalGRDataBase method - Adds and saves data to the local database GuestRegistry
     * @param - String data you wish to save
     * @return - true or false, true meaning the insertion was successful and false meaning it was not
     */
    public boolean addToLocalGRDataBase(String name, String phone, String nccID, String address,
                                        String city, String zipcode, String state, String affiliation,
                                        String age, String gender, String diet, String programs,
                                        String snap, String employment, String health, String housing,
                                        String income, String householdNum, String children1,
                                        String children5, String children12, String children18,
                                        String referralInfo, String additionalInfo,
                                        String nameOfVolunteer, String barcode, String lastVisit)
    {

        if(this.doesExistInLocal(phone))
        {
            return false;
        }

        SQLiteDatabase db = this.getWritableDatabase();

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.getDefault());
        String date = sdf.format(new Date());

        ContentValues values = new ContentValues();
        values.put(NAME, name);
        values.put(PHONE, phone);
        values.put(NCC_ID, nccID);
        values.put(DATE, date);
        values.put(ADDRESS, address);
        values.put(CITY, city);
        values.put(ZIP, zipcode);
        values.put(STATE, state);
        values.put(AFFILIATION, affiliation);
        values.put(AGE, age);
        values.put(GENDER, gender);
        values.put(DIET, diet);
        values.put(PROGRAMS, programs);
        values.put(SNAP, snap);
        values.put(EMPLOYMENT, employment);
        values.put(HEALTH, health);
        values.put(HOUSING, housing);
        values.put(INCOME, income);
        values.put(HOUSEHOLD_NUM, householdNum);
        values.put(CHILDREN_1, children1);
        values.put(CHILDREN_5, children5);
        values.put(CHILDREN_12, children12);
        values.put(CHILDREN_18, children18);
        values.put(BARCODE, barcode);
        values.put(LASTVISIT, lastVisit);

        long result = db.insert(TABLE_NAME,null,values);

        if (result == -1)
        {
            return false;
        }
        else
        {
            return true;
        }

    }



    /**
     * doesExistInLocal - checks if user with a nccid or phonenumber is registered in the local database
     * @param - String data you wish to save
     * @return - true or false, true meaning the phone number or nccid is registered in the local database and false meaning the phonenumber or nccid is not registered.
     */
    public boolean doesExistInLocal(String phoneNum) {


        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM NESTGuestRegistry", new String[]{});

        // Get the index of the "nccID" column and the "phone" column
        // final int NCC_ID2 = cursor.getColumnIndex("nccID");
        final int PHONE_NUMBER = cursor.getColumnIndex("phone");
        final int BAR_CODE = cursor.getColumnIndex("barcode");

        if (( db == null ) || ( !this.isDataInLocal() ) )
        {
            Log.d("##NOLOCALDATA" ,"There is no data in the local database");
            return false;
        }

        for (cursor.moveToFirst(); !cursor.isAfterLast();)
        {

           // Log.d("##Localtestid" ,cursor.getString(NCC_ID2));

            Log.d("##Localtestpn" ,cursor.getString(PHONE_NUMBER));
            if (cursor.getString(PHONE_NUMBER).equals(phoneNum))
            {
                cursor.close();
                return true;
            }

            // Move to the next row
            cursor.moveToNext();
        }
        //make sure to close cursor
        cursor.close();
        return false;
    }


    /**
     * isRegistered - returns the phone number associated with the barcode
     * @param barcode
     * @return associated phone number or null if none found
     */
    public String isRegistered(@NonNull String barcode)
    {
       SQLiteDatabase db = this.getWritableDatabase();

       Cursor cursor = db.rawQuery("SELECT * FROM NESTGuestRegistry", new String[]{});

       final int PHONE_NUMBER = cursor.getColumnIndex("phone");
       final int BAR_CODE = cursor.getColumnIndex("barcode");

        if (( db == null ) || ( !this.isDataInLocal() ) )
        {
            Log.d("##NOLOCALDATA" ,"There is no data in the local database");
            return null;
        }

        for (cursor.moveToFirst(); !cursor.isAfterLast();)
        {
          Log.d("##Localtestbc", cursor.getString(BAR_CODE));
          if (cursor.getString(BAR_CODE).equals(barcode)) {
              String phoneNum = cursor.getString(PHONE_NUMBER);
              cursor.close();
              return phoneNum;
          }

          // Move to the next row
          cursor.moveToNext();
        }

      //make sure to close cursor
      cursor.close();
        return null;
    }

    /**
     * isDataInLocal - checks if there is rows of data in the local database
     * @return - true or false, true if there is more than 0 rows (which means there is data in the local database)
     * in the local database else return false(there is no data in local)
     */
    public boolean isDataInLocal() {


        SQLiteDatabase db = this.getWritableDatabase();

        int nestGuestRegistryRows = 0;

        //run query
        Cursor cursor = db.rawQuery("SELECT COUNT(*)  FROM NESTGuestRegistry",null);

        if (cursor.getCount() > 0)
        {
            cursor.moveToFirst();
            nestGuestRegistryRows = cursor.getInt(0);

        }
        Log.d("##NGRowCount" , String.valueOf(nestGuestRegistryRows));

        if (nestGuestRegistryRows > 0) {
            return true;
        }
        else {
            return false;
        }
    }


    /**
     * deleteTableRow -  deletes a rows of data in the local database (The id of a row does not change
     * when a deletion occurs so check app inspectior before deleting)
     * @param rowid, the int index of the row you wish to delete (id is one based, example: 1,2,3,4)
     * @return - true or false, true deletion was successful and false if not
     */
    public boolean deleteTableRow(int rowid) {

        SQLiteDatabase db = this.getWritableDatabase();
        if ((db == null) || (!this.isDataInLocal()))
        {
            Log.d("##NGRowDeletion" , "There are no rows to be Deleted (database is null)");
            return false;
        }

        int rowsDeleted = db.delete("NESTGuestRegistry","_id=" + rowid,null);

        if (rowsDeleted <= 0)
        {
            Log.d("##NGRowDeletion" , "Deletion UNSuccessful");
            return false;
        }

        Log.d("##NGRowDeletion" , "Deletion Successful");
        return true;

    }

    /**
     * getTotalGuests method - Counts the total number of guests registered with the NEST.
     * @param db - The database you wish to query.
     * @return - THe total number of guests registered with the NEST.
     */
//    public int getTotalGuests(SQLiteDatabase db) {
//        Cursor c = db.query(
//                TABLE_NAME,
//                new String[] {_ID},
//                null,
//                null,
//                null,
//                null,
//                null
//        );
//        int count = 0;
//        while (c.moveToNext())
//            count++;
//        c.close();
//        return count;
//    }

    /**
     * getMonthlyGuests method - Counts the number of guests registered to the NEST in a given month
     * @param db - The database you wish to query
     * @param month - The month you wish to check
     * @param year - The year you wish to check
     * @return - The number of guests registered to the NEST in the given month
     */
//    public int getMonthlyGuests(SQLiteDatabase db, int month, int year) {
//        DecimalFormat monthFormat = new DecimalFormat("00");
//        String queryArgument = monthFormat.format(month) + "__" + year + "%";
//        Cursor c = db.query(
//                TABLE_NAME,
//                new String[] {_ID},
//                DATE + " LIKE ? ",
//                new String[] {queryArgument},
//                null,
//                null,
//                null
//        );
//        int count = 0;
//        while (c.moveToNext())
//            count++;
//        c.close();
//        return count;
//    }

}
