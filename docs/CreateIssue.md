# Creating a New Issue

1.	Create a new issue for each bug and feature enhancement.  Issues should be small enough to be completed in a single work session (more or less).  For each issue:

    1.	Provide a meaningful title.

    2.	Use the appropriate template (bug or feature).  For an example of a completed bug template see closed issue #243 and for an example of a completed feature template see closed issue #240.  

    3.	Add labels.  Your issue should have a Type Label (Type:Bug, Type:Feature, Type:Refactor or Type:Other) and a Component Label (Component:Accounts, Component:CheckExpirationDate, Component:GuestIntake, Component:GuestVisit, Component:PushNotifications).  See **Projection Information, Labels** for descriptions of these labels.

# When working on an Issue
1.	Add licenses to new files

2.	Use commit messages to give credit to code previously written that you are re-using or moving.  For example,
`git commit -m “Copied the scanner layout from activity_scanner.xml to fragment_scan.xml”`
