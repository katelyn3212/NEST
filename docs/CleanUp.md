# Clean Up When Work Is Complete

After completing work and pushing changes to the Nest repository, you will need to make some changes to your local repository. You will want to remove the branch created to do work on and make sure that your main branch is up to date.

Switch to the main branch:

    $ git checkout main

Delete the work branch:

    $ git branch -d <branch name>

Update your main branch to match what is on GitLab:

    $ git pull